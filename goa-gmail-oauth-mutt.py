#!/usr/bin/python3
# SPDX-License-Identifier: GPL-3.0-or-later

import sys
import gi

gi.require_version('GLib', '2.0')
gi.require_version('Gio', '2.0')
from gi.repository import GLib
from gi.repository import Gio

goa_bus_name = "org.gnome.OnlineAccounts"
goa_bus_path = "/org/gnome/OnlineAccounts"

goa_acct_iface = "org.gnome.OnlineAccounts.Account"
goa_oauth2_iface = "org.gnome.OnlineAccounts.OAuth2Based"
goa_mail_iface = "org.gnome.OnlineAccounts.Mail"
dbus_props_iface = "org.freedesktop.DBus.Properties"

if len(sys.argv) > 2:
    print("syntax: %s [IDENTITY-EMAIL]" % sys.argv[0])
    sys.exit(1)

identity = None
if len(sys.argv) == 2:
    identity = sys.argv[1]

def get_managed_objects():
    mgr = Gio.DBusProxy.new_for_bus_sync(Gio.BusType.SESSION,
                                         Gio.DBusProxyFlags.NONE,
                                         None,
                                         goa_bus_name,
                                         goa_bus_path,
                                         "org.freedesktop.DBus.ObjectManager",
                                         None)
    accts = mgr.call_sync('GetManagedObjects',
                          None,
                          Gio.DBusCallFlags.NONE,
                          -1,
                          None)

    return accts.unpack()[0]


def find_google_mail_account_objs():
    acct_objs = get_managed_objects()
    for obj_path, obj_ifaces in acct_objs.items():
        if goa_acct_iface not in obj_ifaces:
            continue
        if goa_mail_iface not in obj_ifaces:
            continue
        if goa_oauth2_iface not in obj_ifaces:
            continue

        acct = obj_ifaces[goa_acct_iface]
        if acct["ProviderType"] == "google":
            yield obj_path, acct["Identity"]

def list_google_mail_identities():
    for obj_path, acct_identity in find_google_mail_account_objs():
        print(acct_identity)

def get_google_mail_account_obj(identity):
    for obj_path, acct_identity in find_google_mail_account_objs():
        if identity is None or acct_identity == identity:
            return obj_path
    return None

def get_gmail_access_token(gmail_obj):
    mgr = Gio.DBusProxy.new_for_bus_sync(Gio.BusType.SESSION,
                                         Gio.DBusProxyFlags.NONE,
                                         None,
                                         goa_bus_name,
                                         gmail_obj,
                                         goa_oauth2_iface,
                                         None)
    token = mgr.call_sync('GetAccessToken',
                          None,
                          Gio.DBusCallFlags.NONE,
                          -1,
                          None)
    return token.unpack()[0]

def get_property(obj, iface, prop):
    mgr = Gio.DBusProxy.new_for_bus_sync(Gio.BusType.SESSION,
                                         Gio.DBusProxyFlags.NONE,
                                         None,
                                         goa_bus_name,
                                         obj,
                                         dbus_props_iface,
                                         None)
    val = mgr.call_sync('Get',
                          GLib.Variant("(ss)", [iface, prop]),
                          Gio.DBusCallFlags.NONE,
                          -1,
                          None)
    return val.unpack()[0]

if identity == "--list":
    list_google_mail_identities()
    sys.exit(0)

gmail_obj = get_google_mail_account_obj(identity)
if gmail_obj is None:
    print("No Google account found for OAuth2 with Mail", file=sys.stderr)
    sys.exit(1)

attn = get_property(gmail_obj, goa_acct_iface, "AttentionNeeded")
if attn:
    print("Attention needed to use this account. Re-login likely required.")
    sys.exit(1)

print (get_gmail_access_token(gmail_obj))
